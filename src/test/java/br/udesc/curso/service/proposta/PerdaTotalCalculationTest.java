package br.udesc.curso.service.proposta;

import static org.junit.Assert.fail;

import java.util.Date;

import org.junit.Assert;
import org.junit.Test;

import br.udesc.curso.model.Cliente;
import br.udesc.curso.model.Veiculo;
import br.udesc.curso.service.proposta.calculation.PerdaTotalCalculation;
import br.udesc.curso.vo.PropostaVo;

public class PerdaTotalCalculationTest {

	@Test
	public void testVidaLoka() {
		Cliente cliente = new Cliente();
		cliente.setNascimento(new Date(2000-1900, 10, 1));
		
		Veiculo veiculo = new Veiculo();
		veiculo.setAnoDeFabricacao(new Date(1995));
		veiculo.setImportado(false);
		veiculo.setBlindado(false);
		veiculo.setValor(10000);
		
		PropostaVo proposta = new PropostaVo();
		proposta.setCliente(cliente);
		proposta.setVeiculo(veiculo);
		
		PerdaTotalCalculation calculation = new PerdaTotalCalculation();
		
		float valor = calculation.calcular(proposta);
		
		Assert.assertEquals(1500, 500, 1000);
	}
}
