package br.udesc.curso.service.proposta.calculation;

import org.springframework.stereotype.Component;

import br.udesc.curso.model.Veiculo;
import br.udesc.curso.vo.PropostaVo;

@Component
public abstract class CoberturaCalculation {
	
	public float calcular(PropostaVo proposta) {
		Veiculo veiculo = proposta.getVeiculo();
		return calcularValor(proposta) + calcularPercentualVeiculo(proposta) * veiculo.getValor();
	}
	
	abstract float calcularValor(PropostaVo proposta);
	
	abstract float calcularPercentualVeiculo(PropostaVo proposta);
}


