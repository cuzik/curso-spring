package br.udesc.curso.service.proposta;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.udesc.curso.enums.Cobertura;
import br.udesc.curso.vo.PropostaVo;

@Service
public class PropostaService {
	
	@Autowired
	private CoberturaCalculationFactory factory;
	
	public float calcular(PropostaVo proposta) {
		float valorTotal = 0;
		
		for (Cobertura cobertura: proposta.getCoberturas()) {			
			valorTotal += (float) factory.build(cobertura).calcular(proposta);
		}
		
		return valorTotal;
	}
}
