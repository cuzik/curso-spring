package br.udesc.curso.service.proposta.calculation;

import java.util.Date;

import org.springframework.stereotype.Component;

import br.udesc.curso.vo.PropostaVo;

@Component
public class VidroCalculation extends CoberturaCalculation{

	@Override
	float calcularValor(PropostaVo proposta) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	float calcularPercentualVeiculo(PropostaVo proposta) {
		Date today = new Date();
		int idadeVeiculo = proposta.getVeiculo().getAnoDeFabricacao().getYear() - today.getYear();
		
		float pct = 0;
		
		if (idadeVeiculo < 3 || idadeVeiculo >= 10) {
			pct = (float) 0.0075;
		} else if (idadeVeiculo < 5) {
			pct = (float) 0.0055;
		} else if (idadeVeiculo < 10) {
			pct = (float) 0.0025;
		}
		
		if (proposta.getVeiculo().isBlindado()) {
			pct += proposta.getVeiculo().isImportado() ? (float) 0.0075 : (float) 0.005;
			
		} else if (proposta.getVeiculo().isImportado()){
			pct += (float) 0.0055;
		}
		
		return pct;
	}
}
