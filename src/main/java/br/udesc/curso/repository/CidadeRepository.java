package br.udesc.curso.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.udesc.curso.model.Cidade;

public interface CidadeRepository
	extends JpaRepository<Cidade, Long>{

}
